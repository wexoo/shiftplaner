package net.wexoo.shiftplaner.ui.calendar;

import java.util.Calendar;
import java.util.TimeZone;

import android.annotation.TargetApi;
import android.content.ContentValues;
import android.os.Build;
import android.provider.CalendarContract.Events;

public class CalendarEvent {
	
	private String title;
	private String descr;
	private String location;
	private long startTime;
	private long endTime;
	private String calendarId;
	
	public CalendarEvent() {
	}
	
	public CalendarEvent(String title, String descr, String location, long startTime, long endTime, String idCalendar) {
		this.title = title;
		this.descr = descr;
		this.location = location;
		this.startTime = startTime;
		this.endTime = endTime;
		calendarId = idCalendar;
	}
	
	public static ContentValues toContentValues(CalendarEvent evt) {
		ContentValues cv = new ContentValues();
		cv.put("calendar_id", evt.getCalendarId());
		cv.put("title", evt.getTitle());
		cv.put("description", evt.getDescr());
		cv.put("eventLocation", evt.getLocation());
		cv.put("dtstart", evt.getStartTime());
		cv.put("dtend", evt.getEndTime());
		cv.put("eventStatus", 1);
		cv.put("visibility", 0);
		cv.put("transparency", 0);
		
		return cv;
		
	}
	
	@TargetApi(Build.VERSION_CODES.ICE_CREAM_SANDWICH)
	public static ContentValues toICSContentValues(CalendarEvent evt) {
		
		ContentValues cv = new ContentValues();
		cv.put(Events.CALENDAR_ID, evt.getCalendarId());
		cv.put(Events.TITLE, evt.getTitle());
		cv.put(Events.DESCRIPTION, evt.getDescr());
		cv.put(Events.EVENT_LOCATION, evt.getLocation());
		cv.put(Events.DTSTART, evt.getStartTime());
		cv.put(Events.DTEND, evt.getEndTime());
		
		Calendar cal = Calendar.getInstance();
		TimeZone tz = cal.getTimeZone();
		
		cv.put(Events.EVENT_TIMEZONE, tz.getDisplayName());
		/*
		cv.put(Events.STATUS, 1);
		cv.put(Events.VISIBLE, 0);
		cv.put("transparency", 0);
		*/
		
		return cv;
	}
	
	public String getTitle() {
		return title;
	}
	
	public void setTitle(String title) {
		this.title = title;
	}
	
	public String getDescr() {
		return descr;
	}
	
	public void setDescr(String descr) {
		this.descr = descr;
	}
	
	public String getLocation() {
		return location;
	}
	
	public void setLocation(String location) {
		this.location = location;
	}
	
	public long getStartTime() {
		return startTime;
	}
	
	public void setStartTime(long startTime) {
		this.startTime = startTime;
	}
	
	public long getEndTime() {
		return endTime;
	}
	
	public void setEndTime(long endTime) {
		this.endTime = endTime;
	}
	
	public String getCalendarId() {
		return calendarId;
	}
	
	public void setCalendarId(String calendarId) {
		this.calendarId = calendarId;
	}
	
	@Override
	public String toString() {
		return "CalendarEvent [title=" + title + ", descr=" + descr + ", location=" + location + ", startTime=" + startTime
					+ ", endTime=" + endTime + ", idCalendar=" + calendarId + "]";
	}
}