package net.wexoo.shiftplaner.ui;

import java.util.Calendar;
import java.util.GregorianCalendar;

import net.simonvt.calendarview.CalendarView;
import net.simonvt.calendarview.CalendarView.OnDateChangeListener;
import net.wexoo.organicdroid.base.BaseApplication;
import net.wexoo.shiftplaner.R;
import net.wexoo.shiftplaner.ui.base.SuperActivity;
import android.os.Bundle;
import android.util.Log;

public class MainActivity extends SuperActivity implements OnDateChangeListener {
	
	private static final String TAG = MainActivity.class.getSimpleName();
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		BaseApplication.mainContext = this;
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.activity_main);
		
		CalendarView calView = (CalendarView) findViewById(R.id.calendar_view);
		calView.setOnDateChangeListener(this);
	}
	
	@Override
	public void onSelectedDayChange(CalendarView view, int year, int month, int dayOfMonth) {
		Calendar cal = new GregorianCalendar(year, month, dayOfMonth);
		Log.d(TAG, "Date selected: " + cal.getTime().toString());
		// UIUtil.showShortToast(this, cal.getTime().toString());
	}
}