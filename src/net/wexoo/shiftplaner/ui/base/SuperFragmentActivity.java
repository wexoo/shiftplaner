package net.wexoo.shiftplaner.ui.base;

import java.util.GregorianCalendar;

import net.wexoo.organicdroid.base.BaseFragmentActivity;
import net.wexoo.organicdroid.slidemenu.BaseSlideMenuView;
import net.wexoo.organicdroid.util.UIUtil;
import net.wexoo.shiftplaner.R;
import net.wexoo.shiftplaner.ui.MainActivity;
import net.wexoo.shiftplaner.ui.slidemenu.SlideMenuView;
import android.annotation.SuppressLint;
import android.content.ContentResolver;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.provider.CalendarContract;
import android.provider.CalendarContract.Calendars;
import android.provider.CalendarContract.Events;
import android.view.View;

@SuppressLint("InlinedApi")
public class SuperFragmentActivity extends BaseFragmentActivity {
	
	public static final String[] EVENT_PROJECTION = new String[] { //
	Calendars._ID, // 0
				Calendars.ACCOUNT_NAME, // 1
				Calendars.CALENDAR_DISPLAY_NAME // 2
	};
	
	private static final int PROJECTION_DISPLAY_NAME_INDEX = 2;
	
	@Override
	public void onHomeClick(View v) {
		UIUtil.goToActivityAndClearTop(this, MainActivity.class);
	}
	
	@Override
	protected BaseSlideMenuView createSlideMenuView() {
		return new SlideMenuView(this);
	}
	
	/**
	 * TODO: build API security mechanism - only API 14+ supports event creation and querying this way
	 */
	@SuppressLint("NewApi")
	@Override
	protected void doAction(Integer idOfElement) {
		switch (idOfElement) {
			case R.id.b_create_event:
				Intent intent = new Intent(Intent.ACTION_INSERT);
				intent.setType("vnd.android.cursor.item/event");
				intent.putExtra(Events.TITLE, "Learn Android");
				intent.putExtra(Events.EVENT_LOCATION, "Home suit home");
				intent.putExtra(Events.DESCRIPTION, "Download Examples");
				
				GregorianCalendar calDate = new GregorianCalendar(2012, 10, 02);
				intent.putExtra(CalendarContract.EXTRA_EVENT_BEGIN_TIME, calDate.getTimeInMillis());
				intent.putExtra(CalendarContract.EXTRA_EVENT_END_TIME, calDate.getTimeInMillis());
				
				intent.putExtra(CalendarContract.EXTRA_EVENT_ALL_DAY, true);
				
				intent.putExtra(Events.RRULE, "FREQ=WEEKLY;COUNT=11;WKST=SU;BYDAY=TU,TH");
				
				intent.putExtra(Events.ACCESS_LEVEL, Events.ACCESS_PRIVATE);
				intent.putExtra(Events.AVAILABILITY, Events.AVAILABILITY_BUSY);
				
				startActivity(intent);
				
				// if (Build.VERSION.SDK_INT >= 14) {
				// Intent intent = new Intent(Intent.ACTION_INSERT)
				// .setData(Events.CONTENT_URI)
				// .putExtra(CalendarContract.EXTRA_EVENT_BEGIN_TIME, beginTime.getTimeInMillis())
				// .putExtra(CalendarContract.EXTRA_EVENT_END_TIME, endTime.getTimeInMillis())
				// .putExtra(Events.TITLE, "Yoga")
				// .putExtra(Events.DESCRIPTION, "Group class")
				// .putExtra(Events.EVENT_LOCATION, "The gym")
				// .putExtra(Events.AVAILABILITY, Events.AVAILABILITY_BUSY)
				// .putExtra(Intent.EXTRA_EMAIL, "rowan@example.com,trevor@example.com");
				// startActivity(intent);
				// } else {
				// Calendar cal = Calendar.getInstance();
				// Intent intent = new Intent(Intent.ACTION_EDIT);
				// intent.setType("vnd.android.cursor.item/event");
				// intent.putExtra("beginTime", cal.getTimeInMillis());
				// intent.putExtra("allDay", true);
				// intent.putExtra("rrule", "FREQ=YEARLY");
				// intent.putExtra("endTime", cal.getTimeInMillis()+60*60*1000);
				// intent.putExtra("title", "A Test Event from android app");
				// startActivity(intent);
				// }
				break;
			case R.id.b_query_calender:
				Cursor cur = null;
				ContentResolver cr = getContentResolver();
				Uri uri = Calendars.CONTENT_URI;
				String selection = "((" + Calendars.ACCOUNT_NAME + " = ?) AND (" + Calendars.ACCOUNT_TYPE + " = ?))";
				
				String[] selectionArgs = new String[] {"p.weixlbaumer@gmail.com", "com.google"};
				cur = cr.query(uri, EVENT_PROJECTION, selection, selectionArgs, null);
				
				while (cur.moveToNext()) {
					String displayName = null;
					displayName = cur.getString(PROJECTION_DISPLAY_NAME_INDEX);
					UIUtil.showShortToast(this, "Calendar " + displayName);
				}
				break;
		}
	}
}
