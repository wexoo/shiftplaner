package net.wexoo.shiftplaner.ui;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import net.wexoo.organicdroid.Log;
import net.wexoo.organicdroid.base.BaseApplication;
import net.wexoo.shiftplaner.R;
import net.wexoo.shiftplaner.ui.base.SuperFragmentActivity;
import net.wexoo.shiftplaner.ui.calendar.CalendarEvent;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.CalendarContract;
import android.provider.CalendarContract.Events;
import android.support.v4.app.LoaderManager.LoaderCallbacks;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.view.View;

@TargetApi(Build.VERSION_CODES.ICE_CREAM_SANDWICH)
public class ShiftPlanActivity extends SuperFragmentActivity implements LoaderCallbacks<Cursor> {
	
	private static final String TAG = ShiftPlanActivity.class.getSimpleName();
	private static final int BASE_LOADER = 0;
	private Context ctx;
	private String baseUri;
	private String[] projections = new String[] {CalendarContract.Calendars._ID, CalendarContract.Calendars.ACCOUNT_NAME,
				CalendarContract.Calendars.CALENDAR_DISPLAY_NAME, CalendarContract.Calendars.NAME,
				CalendarContract.Calendars.CALENDAR_COLOR};;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		BaseApplication.mainContext = this;
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.shift_plan_activity);
		
		setContext(this);
		
		getSupportLoaderManager().initLoader(BASE_LOADER, null, this);
	}
	
	public void setContext(Context context) {
		ctx = context;
		baseUri = getCalendarUriBase();
	}
	
	public void onStartClick(final View view) {
		switch (view.getId()) {
			case R.id.b_start_export:
				addEvent(buildNewCalendarEvent());
				break;
			default:
				break;
		}
	}
	
	private CalendarEvent buildNewCalendarEvent() {
		CalendarEvent evt = new CalendarEvent();
		evt.setTitle("Test Event");
		evt.setDescr("Test desc");
		evt.setLocation("test location");
		evt.setStartTime(new Date().getTime());
		GregorianCalendar newCalendar = new GregorianCalendar();
		newCalendar.add(Calendar.DAY_OF_YEAR, 3);
		evt.setEndTime(newCalendar.getTimeInMillis());
		evt.setCalendarId("1");
		return evt;
	}
	
	@SuppressLint("NewApi")
	public void addEvent(CalendarEvent evt) {
		Log.d(TAG, "Insert event [" + evt + "]");
		
		if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.ICE_CREAM_SANDWICH) {
			ContentResolver cr = ctx.getContentResolver();
			Uri uri = cr.insert(Events.CONTENT_URI, CalendarEvent.toICSContentValues(evt));
			System.out.println("Event URI [" + uri + "]");
		} else {
			try {
				Uri evtUri = ctx.getContentResolver().insert(getCalendarUri("events"), CalendarEvent.toContentValues(evt));
				Log.d(TAG, "" + evtUri);
			} catch (Throwable t) {
				Log.e(TAG, t.getLocalizedMessage());
			}
		}
	}
	
	private Uri getCalendarUri(String path) {
		return Uri.parse(baseUri + "/" + path);
	}
	
	private String getCalendarUriBase() {
		String calendarUriBase = null;
		Uri calendars = Uri.parse("content://calendar/calendars");
		Cursor managedCursor = null;
		try {
			managedCursor = ctx.getContentResolver().query(calendars, null, null, null, null);
		} catch (Exception e) {
			// e.printStackTrace();
		}
		
		if (managedCursor != null) {
			calendarUriBase = "content://calendar/";
		} else {
			calendars = Uri.parse("content://com.android.calendar/calendars");
			try {
				managedCursor = ctx.getContentResolver().query(calendars, null, null, null, null);
			} catch (Exception e) {
				// e.printStackTrace();
			}
			
			if (managedCursor != null) {
				calendarUriBase = "content://com.android.calendar/";
			}
		}
		
		Log.d(TAG, "URI [" + calendarUriBase + "]");
		return calendarUriBase;
	}
	
	@Override
	public Loader<Cursor> onCreateLoader(int loaderID, Bundle bundle) {
		
		switch (loaderID) {
			case BASE_LOADER:
				// Returns a new CursorLoader
				return new CursorLoader(ctx, // Parent activity context
							CalendarContract.Calendars.CONTENT_URI, // Table to query
							projections, // Projection to return
							null, // No selection clause
							null, // No selection arguments
							null // Default sort order
				);
			default:
				// An invalid id was passed in
				return null;
		}
	}
	
	@Override
	public void onLoadFinished(Loader<Cursor> cursorLoader, Cursor cursor) {
		// Log.d(TAG, "Calendarcursor: " + cursor);
		
		if (cursor.moveToFirst()) {
			do {
				for (String projection : projections) {
					Log.d(TAG, "projection: " + projection + " - value: " + cursor.getString(cursor.getColumnIndex(projection)));
				}
			} while (cursor.moveToNext());
		}
		// cursor.close();
		
	}
	
	@Override
	public void onLoaderReset(Loader<Cursor> arg0) {
		// TODO Auto-generated method stub
		
	}
}